﻿INSERT INTO staff (name,surname,post,rate,id_code) VALUES 
('Nick','cave','Director','10000','1234567890'),
('ann','Wiseberg','Bookkeeper','3000','0987654321'),
('Peter','Norton','Security guard','200','9876543210'),
('Kate','Rouning','Sales manager','100000','0123456789'),
('Nick','Cave','Sales manager','1000','0123456789');

INSERT INTO suppliers(country,concern,contacts) VALUES
('USA','Cadillac','www.cadillac.com'),
('USA','Cadillac','e-mail: chevrolet@gmail.com');

INSERT INTO suppliers(country,concern) VALUES ('Germany','Audi');

INSERT INTO delivery(supplier_id,number,date,cost) VALUES
(1,20,'2013-01-22','225000'),
(2,10,'2013-04-15','20000000'),
(3,7,'2013-03-06','60000'),
(3,4,'2013-03-06','70000');

INSERT INTO client(name,surname,passport_number) VALUES
('Antony','Girlich','GT894523'),
('Kris','Braun','GT894523'),
('tanya','Petrova','FD741852'),
('Vlad','tarek','VB475869');

INSERT INTO presentation(venue,time,cost) VALUES
('Lichten street, 22','2013-05-13 17:00','60000'),
('Notis avenue, 48','2013-03-18 19:00','15000'),
('Freed street, 15','2013-03-18 19:00','25000');

INSERT INTO client_presentation(client_id,presentation_id) VALUES
('3','1'),
('3','1'),
('4','5'),
('5','3');

INSERT INTO products(delivery_id,mark,model,body_type,transmission,fuel,serial_number,price) VALUES
(1,'caddilac','escalade','SUV','automatic','oil','A125FGH09965LPRFJ','110000'),
(3,'chevrolet','camaro','sedan','automanual','gasoline','J56UUFH09965LPRFJ','70000'),
(1,'caddilac','CTS','desan','automatic','gasoline','A1259990K965LPRFJ','60000'),
(4,'caddilac','CTS-V','sedan','automatic','gasoline','UYWER546NVS58TR96','75000'),
(2,'audi','R8','sedan','robotized','gasoline','UYWER546NVS58TR96','21000000');

INSERT INTO presentation_product(presentation_id,product_id) VALUES
('1','2'),
('1','2'),
('5','3'),
('3','7');

INSERT INTO test_drive(product_id,client_id,distance,date) VALUES
(5,300,'2013-05-10'),
(1,4,15,'2013-02-20'),
(1,4,20,'2013-04-09');

INSERT INTO guarantee(term,mileage_km,details) VALUES
(INTERVAL '4 years 6 months','100000','guarantee does not cover the replacement of worn parts, wear and tear which occurred in the course of normal operation of the vehicle'),
(INTERVAL '2 years','200000','guarantee does not cover the replacement of worn parts, wear and tear which occurred in the course of normal operation of the vehicle'),
(INTERVAL '2 years','80000','guarantee does not cover the replacement of worn parts, wear and tear which occurred in the course of normal operation of the vehicle');

INSERT INTO sale(product_id,client_id,guarantee_id,staff_id,date) VALUES
(1,1,1,4,'2013-03-15'),
(1,2,2,5,'2013-04-12'),
(8,3,3,1,'2013-02-09'),
(4,6,1,4,'2013-05-03'),
(5,1,4,5,'2013-05-27');