﻿INSERT INTO staff (name,surname,post,rate,id_code) VALUES 
('Nick','Cave','Director','10000','1234567890'),
('Ann','Wiseberg','Bookkeeper','3000','0987654321'),
('Peter','Norton','Security guard','1500','9876543210'),
('Kate','Rouning','Sales manager','1000','0123456789'),
('Nick','Cave','Sales manager','1000','1237890456');

INSERT INTO suppliers(country,concern,contacts) VALUES
('USA','Cadillac','www.cadillac.com'),
('Germany','Audi','tel.+5002561425565'),
('USA','Chevrolet','e-mail: chevrolet@gmail.com');

INSERT INTO delivery(supplier_id,number,date,cost) VALUES
(1,5,'2013-01-22','225000'),
(2,10,'2013-04-15','200000'),
(3,7,'2013-03-06','60000'),
(1,4,'2013-04-19','70000');

INSERT INTO client(name,surname,passport_number) VALUES
('Antony','Girlich','GT894523'),
('Kris','Braun','KU123456'),
('Tanya','Petrova','FD741852'),
('Vlad','Tarek','VB475869');

INSERT INTO presentation(venue,time,cost) VALUES
('Lichten street, 22','2013-05-13 17:00','20000'),
('Notis avenue, 48','2013-03-18 19:00','15000'),
('Freed street, 15','2013-04-26 18:00','25000');

INSERT INTO client_presentation(client_id,presentation_id) VALUES
('3','1'),
('2','1'),
('4','2'),
('1','3');

INSERT INTO products(delivery_id,mark,model,body_type,transmission,fuel,serial_number,price) VALUES
(1,'caddilac','escalade','SUV','automatic','gasoline','A125FGH09965LPRFJ','110000'),
(3,'chevrolet','camaro','sedan','manual','gasoline','J56UUFH09965LPRFJ','70000'),
(1,'caddilac','CTS','sedan','automatic','gasoline','A1259990K965LPRFJ','60000'),
(4,'caddilac','CTS-V','sedan','automatic','gasoline','A1259L8PK965LPRFJ','75000'),
(2,'audi','R8','sedan','robotized','gasoline','UYWER546NVS58TR96','210000');

INSERT INTO presentation_product(presentation_id,product_id) VALUES
('1','2'),
('2','1'),
('2','3'),
('3','5');

INSERT INTO test_drive(product_id,client_id,distance,date) VALUES
(5,1,30,'2013-05-10'),
(1,4,15,'2013-02-20'),
(2,2,20,'2013-04-09');

INSERT INTO guarantee(term,mileage_km,details) VALUES
(INTERVAL '2 years 6 months','100000','guarantee does not cover the replacement of worn parts, wear and tear which occurred in the course of normal operation of the vehicle'),
(INTERVAL '2 years','70000','guarantee does not cover the replacement of worn parts, wear and tear which occurred in the course of normal operation of the vehicle'),
(INTERVAL '2 years','80000','guarantee does not cover the replacement of worn parts, wear and tear which occurred in the course of normal operation of the vehicle');

INSERT INTO sale(product_id,client_id,guarantee_id,staff_id,date) VALUES
(1,1,1,4,'2013-03-15'),
(2,2,2,5,'2013-04-12'),
(3,3,3,1,'2013-02-09'),
(4,4,1,4,'2013-05-03'),
(5,1,2,5,'2013-05-27');